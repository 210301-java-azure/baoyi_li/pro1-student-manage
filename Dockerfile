FROM java:8
COPY build/libs/testHib-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar testHib-1.0-SNAPSHOT.jar
