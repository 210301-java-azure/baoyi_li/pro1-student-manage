package dev.li.service;

import dev.li.dao.GradeDao;
import dev.li.dao.GradeDaoImpl;
import dev.li.domain.GradeReport;

import java.util.List;

public class GradeService {
    private final GradeDao gradeDao = new GradeDaoImpl();

    public List<GradeReport> getAll(){
        return gradeDao.getAllGrades();
    }

    public GradeReport findById(int gradeId){
        return gradeDao.findById(gradeId);
    }

    public GradeReport insertGrade(GradeReport gradeReport){
        return gradeDao.insertGrade(gradeReport);
    }

    public void delete(int gradeId){
        gradeDao.deleteById(gradeId);
    }
}
